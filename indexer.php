<?php

use Commando\Command;

chdir(__DIR__);

require 'vendor/autoload.php';

set_error_handler('errorHandler');

function errorHandler($severity, $message, $filename, $lineNo) {
    if (error_reporting() == 0) {
        return;
    }

    if (error_reporting() & $severity) {
        throw new ErrorException($message, 0, $severity, $filename, $lineNo);
    }
}

$config = include 'config/config.default.php';

$env = getenv('ENVIRONMENT') ?: 'local';
$envConfigFileName = 'config/config.' . $env . '.php';

if (file_exists($envConfigFileName)) {
    $envConfig = include $envConfigFileName;

    $config = array_replace_recursive($config, $envConfig);
}

$agentCmd = new Command();

$agentCmd
    ->option('a')
    ->aka('all')
    ->describe('When given, this will index all data, rather than just incremental. This should be run on first start.')
    ->boolean();

$agentCmd
    ->option('i')
    ->aka('inc')
    ->aka('incremental')
    ->describe('Do incremental updates. This is the default option.')
    ->boolean();

$indexer = new \Updashd\SearchIndexer($config);

if ($agentCmd['all']) {
    $indexer->setIncludeAll();
    $indexer->runOnce();
}

if ($agentCmd['inc']) {
    $indexer->setIncludeAll(false);
    $indexer->run();
}
