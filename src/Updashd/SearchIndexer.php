<?php
namespace Updashd;

use Elasticsearch\ClientBuilder;
use Updashd\SearchIndexer\Document\AbstractDoctrineDocument;
use Updashd\SearchIndexer\Document\AccountNotifier as AccountNotifierDocument;
use Updashd\SearchIndexer\Document\Environment as EnvironmentDocument;
use Updashd\SearchIndexer\Document\Node as NodeDocument;
use Updashd\SearchIndexer\Document\NodeService as NodeServiceDocument;
use Updashd\SearchIndexer\Document\Incident as IncidentDocument;

class SearchIndexer {
    const INDEX_NAME = 'search';

    protected $includeAll = false;
    private $config;
    private $doctrineManager;
    private $elasticSearchClient;
    private $documents = [];

    public function __construct ($config) {
        // Store the configuration
        $this->setConfig($config);

        $doctrineManager = new Doctrine\Manager($config['doctrine']);
        $this->setDoctrineManager($doctrineManager);

        $client = ClientBuilder::fromConfig($config['elasticsearch']);
        $this->setElasticSearchClient($client);
    }

    private function init () {
        $em = $this->getDoctrineManager()->getEntityManager();
        $client = $this->getElasticSearchClient();

        $this->documents[] = new EnvironmentDocument($em);
        $this->documents[] = new IncidentDocument($em);
        $this->documents[] = new NodeDocument($em);
        $this->documents[] = new NodeServiceDocument($em);
        $this->documents[] = new AccountNotifierDocument($em);

        $typeMappings = [];

        /** @var AbstractDoctrineDocument $document */
        foreach ($this->documents as $document) {
            // Set whether all records should be included
            $document->setUpdateAll($this->getIncludeAll());

            // Initialize mapping structure
            $type = $document->getName();
            $typeMappings[$type]['properties'] = $document->getMappingArray();
        }

        $params = [
            'index' => self::INDEX_NAME,
            'body' => [
                'settings' => [
                    'number_of_shards' => 1,
                    'number_of_replicas' => 0
                ],
                'mappings' => $typeMappings
            ]
        ];

        // TODO: Change this to only create fields that are missing.
        if ($client->indices()->exists(['index' => self::INDEX_NAME])) {
            // TODO: Don't delete, but rather update mappings
            if ($this->getIncludeAll()) {
                $client->indices()->delete(['index' => self::INDEX_NAME]);
                $client->indices()->create($params);
            }
        }
        else {
            $client->indices()->create($params);
        }

    }

    /**
     * @return int count of how many records were updated
     */
    private function process () {
        $client = $this->getElasticSearchClient();

        $params = ['body' => []];

        $count = 0;

        /** @var AbstractDoctrineDocument $document */
        foreach ($this->documents as $document) {
            $type = $document->getName();
            $changes = $document->getUpserts();

            foreach ($changes as $row) {
                $params['body'][] = [
                    'index' => [
                        '_index' => 'search',
                        '_type' => $type,
                        '_id' => $row['id']
                    ]
                ];

                unset($row['id']);

                $params['body'][] = $row;

                // Every 1000 documents stop and send the bulk request
                if ($count % 1000 == 0) {
                    $responses = $client->bulk($params);

                    $this->printResponses($responses);

                    // erase the old bulk request
                    $params = ['body' => []];

                    // unset the bulk response when you are done to save memory
                    unset($responses);
                }

                $count++;
            }
        }

        if (! empty($params['body'])) {
            $responses = $client->bulk($params);
            $this->printResponses($responses);
        }

        printf("time: %s, count: %d\n", date('Y-m-d H:i:s'), $count);

        return $count;
    }

    public function printResponses ($responses) {
        $errors = $responses['errors'];
        $items = $responses['items'];

        echo 'Status: ' . ($errors ? 'Errors' : 'Success') . PHP_EOL;

        foreach ($items as $item) {
            $props = $item['index'];
            $result = $this->getProperty($props, 'result');

            if (! $result) {
                print_r($item);
            }
        }
    }

    protected function getProperty ($array, $key) {
        return is_array($array) && array_key_exists($key, $array) ? $array[$key] : null;
    }

    public function runOnce () {
        try {
            $this->init();

            $this->process();

            $sleepTime = (int) $this->getConfig('frequency');

            sleep($sleepTime);
        }
        catch (\Exception $e) {
            echo $e->getMessage() . PHP_EOL;
            echo $e->getTraceAsString() . PHP_EOL;
        }
    }

    /**
     * Run the worker
     * @param $running
     */
    public function run (&$running = true) {
        try {
            $this->init();

            while ($running) {
                $this->process();

                $sleepTime = (int) $this->getConfig('frequency');

                sleep($sleepTime);
            }
        }
        catch (\Exception $e) {
            echo $e->getMessage() . PHP_EOL;
            echo $e->getTraceAsString() . PHP_EOL;
        }
    }

    /**
     * Retrieve the config, or a key from it.
     * @param string $key
     * @return array
     */
    public function getConfig ($key = null) {
        if ($key && array_key_exists($key, $this->config)) {
            return $this->config[$key];
        }

        return $this->config;
    }

    /**
     * @param array $config
     */
    public function setConfig ($config) {
        $this->config = $config;
    }

    /**
     * @return Doctrine\Manager
     */
    public function getDoctrineManager () {
        return $this->doctrineManager;
    }

    /**
     * @param Doctrine\Manager $doctrineManager
     */
    public function setDoctrineManager ($doctrineManager) {
        $this->doctrineManager = $doctrineManager;
    }

    /**
     * @return \Elasticsearch\Client
     */
    public function getElasticSearchClient () {
        return $this->elasticSearchClient;
    }

    /**
     * @param \Elasticsearch\Client $elasticSearchClient
     */
    public function setElasticSearchClient ($elasticSearchClient) {
        $this->elasticSearchClient = $elasticSearchClient;
    }

    public function setIncludeAll ($includeAll = true) {
        $this->includeAll = $includeAll;
    }

    public function getIncludeAll () {
        return $this->includeAll;
    }

}