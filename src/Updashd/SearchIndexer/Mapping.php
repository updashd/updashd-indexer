<?php
namespace Updashd\SearchIndexer;

class Mapping {
    const TYPE_STRING_TEXT = 'text';
    const TYPE_STRING_KEYWORD = 'keyword';
    const TYPE_NUMERIC_LONG = 'long';
    const TYPE_NUMERIC_INTEGER = 'integer';
    const TYPE_NUMERIC_SHORT = 'short';
    const TYPE_NUMERIC_BYTE = 'byte';
    const TYPE_NUMERIC_DOUBLE = 'double';
    const TYPE_NUMERIC_FLOAT = 'float';
    const TYPE_NUMERIC_HALF_FLOAT = 'half_float';
    const TYPE_NUMERIC_SCALED_FLOAT = 'scaled_float';
    const TYPE_DATE = 'date';
    const TYPE_BOOLEAN = 'boolean';
    const TYPE_BINARY = 'binary';
    const TYPE_RANGE_INTEGER = 'integer_range';
    const TYPE_RANGE_FLOAT = 'float_range';
    const TYPE_RANGE_LONG = 'long_range';
    const TYPE_RANGE_DOUBLE = 'double_range';
    const TYPE_RANGE_DATE = 'date_range';
    const TYPE_OBJECT = 'object';
    const TYPE_NESTED = 'nested';
    const TYPE_GEO_POINT = 'geo_point';
    const TYPE_GEO_SHAPE = 'geo_shape';
    const TYPE_IP = 'ip';
    const TYPE_COMPLETION = 'completion';
    const TYPE_TOKEN_COUNT = 'token_count';

    const ANALYZER_STANDARD = 'standard';
    const ANALYZER_SIMPLE = 'simple';
    const ANALYZER_WHITESPACE = 'whitespace';
    const ANALYZER_STOP = 'stop';
    const ANALYZER_KEYWORD = 'keyword';
    const ANALYZER_PATTERN = 'pattern';
    const ANALYZER_FINGERPRINT = 'fingerprint';

    const ANALYZER_LANGUAGE_ARABIC = 'arabic';
    const ANALYZER_LANGUAGE_ARMENIAN = 'armenian';
    const ANALYZER_LANGUAGE_BASQUE = 'basque';
    const ANALYZER_LANGUAGE_BRAZILIAN = 'brazilian';
    const ANALYZER_LANGUAGE_BULGARIAN = 'bulgarian';
    const ANALYZER_LANGUAGE_CATALAN = 'catalan';
    const ANALYZER_LANGUAGE_CJK = 'cjk';
    const ANALYZER_LANGUAGE_CZECH = 'czech';
    const ANALYZER_LANGUAGE_DANISH = 'danish';
    const ANALYZER_LANGUAGE_DUTCH = 'dutch';
    const ANALYZER_LANGUAGE_ENGLISH = 'english';
    const ANALYZER_LANGUAGE_FINNISH = 'finnish';
    const ANALYZER_LANGUAGE_FRENCH = 'french';
    const ANALYZER_LANGUAGE_GALICIAN = 'galician';
    const ANALYZER_LANGUAGE_GERMAN = 'german';
    const ANALYZER_LANGUAGE_GREEK = 'greek';
    const ANALYZER_LANGUAGE_HINDI = 'hindi';
    const ANALYZER_LANGUAGE_HUNGARIAN = 'hungarian';
    const ANALYZER_LANGUAGE_INDONESIAN = 'indonesian';
    const ANALYZER_LANGUAGE_IRISH = 'irish';
    const ANALYZER_LANGUAGE_ITALIAN = 'italian';
    const ANALYZER_LANGUAGE_LATVIAN = 'latvian';
    const ANALYZER_LANGUAGE_LITHUANIAN = 'lithuanian';
    const ANALYZER_LANGUAGE_NORWEGIAN = 'norwegian';
    const ANALYZER_LANGUAGE_PERSIAN = 'persian';
    const ANALYZER_LANGUAGE_PORTUGUESE = 'portuguese';
    const ANALYZER_LANGUAGE_ROMANIAN = 'romanian';
    const ANALYZER_LANGUAGE_RUSSIAN = 'russian';
    const ANALYZER_LANGUAGE_SORANI = 'sorani';
    const ANALYZER_LANGUAGE_SPANISH = 'spanish';
    const ANALYZER_LANGUAGE_SWEDISH = 'swedish';
    const ANALYZER_LANGUAGE_TURKISH = 'turkish';
    const ANALYZER_LANGUAGE_THAI = 'thai';

    const FORMAT_EPOCH_MILLIS = 'epoch_millis';
    const FORMAT_EPOCH_SECOND = 'epoch_second';
    const FORMAT_DATE_OPTIONAL_TIME = 'date_optional_time';
    const FORMAT_STRICT_DATE_OPTIONAL_TIME = 'strict_date_optional_time';
    const FORMAT_BASIC_DATE = 'basic_date';
    const FORMAT_BASIC_DATE_TIME = 'basic_date_time';
    const FORMAT_BASIC_DATE_TIME_NO_MILLIS = 'basic_date_time_no_millis';
    const FORMAT_BASIC_ORDINAL_DATE = 'basic_ordinal_date';
    const FORMAT_BASIC_ORDINAL_DATE_TIME = 'basic_ordinal_date_time';
    const FORMAT_BASIC_ORDINAL_DATE_TIME_NO_MILLIS = 'basic_ordinal_date_time_no_millis';
    const FORMAT_BASIC_TIME = 'basic_time';
    const FORMAT_BASIC_TIME_NO_MILLIS = 'basic_time_no_millis';
    const FORMAT_BASIC_T_TIME = 'basic_t_time';
    const FORMAT_BASIC_T_TIME_NO_MILLIS = 'basic_t_time_no_millis';
    const FORMAT_BASIC_WEEK_DATE = 'basic_week_date';
    const FORMAT_STRICT_BASIC_WEEK_DATE = 'strict_basic_week_date';
    const FORMAT_BASIC_WEEK_DATE_TIME = 'basic_week_date_time';
    const FORMAT_STRICT_BASIC_WEEK_DATE_TIME = 'strict_basic_week_date_time';
    const FORMAT_BASIC_WEEK_DATE_TIME_NO_MILLIS = 'basic_week_date_time_no_millis';
    const FORMAT_STRICT_BASIC_WEEK_DATE_TIME_NO_MILLIS = 'strict_basic_week_date_time_no_millis';
    const FORMAT_DATE = 'date';
    const FORMAT_STRICT_DATE = 'strict_date';
    const FORMAT_DATE_HOUR = 'date_hour';
    const FORMAT_STRICT_DATE_HOUR = 'strict_date_hour';
    const FORMAT_DATE_HOUR_MINUTE = 'date_hour_minute';
    const FORMAT_STRICT_DATE_HOUR_MINUTE = 'strict_date_hour_minute';
    const FORMAT_DATE_HOUR_MINUTE_SECOND = 'date_hour_minute_second';
    const FORMAT_STRICT_DATE_HOUR_MINUTE_SECOND = 'strict_date_hour_minute_second';
    const FORMAT_DATE_HOUR_MINUTE_SECOND_FRACTION = 'date_hour_minute_second_fraction';
    const FORMAT_STRICT_DATE_HOUR_MINUTE_SECOND_FRACTION = 'strict_date_hour_minute_second_fraction';
    const FORMAT_DATE_HOUR_MINUTE_SECOND_MILLIS = 'date_hour_minute_second_millis';
    const FORMAT_STRICT_DATE_HOUR_MINUTE_SECOND_MILLIS = 'strict_date_hour_minute_second_millis';
    const FORMAT_STRICT_DATE_TIME = 'strict_date_time';
    const FORMAT_DATE_TIME = 'date_time';
    const FORMAT_STRICT_DATE_TIME_NO_MILLIS = 'strict_date_time_no_millis';
    const FORMAT_DATE_TIME_NO_MILLIS = 'date_time_no_millis';
    const FORMAT_STRICT_HOUR = 'strict_hour';
    const FORMAT_HOUR = 'hour';
    const FORMAT_HOUR_MINUTE = 'hour_minute';
    const FORMAT_STRICT_HOUR_MINUTE = 'strict_hour_minute';
    const FORMAT_HOUR_MINUTE_SECOND = 'hour_minute_second';
    const FORMAT_STRICT_HOUR_MINUTE_SECOND = 'strict_hour_minute_second';
    const FORMAT_HOUR_MINUTE_SECOND_FRACTION = 'hour_minute_second_fraction';
    const FORMAT_STRICT_HOUR_MINUTE_SECOND_FRACTION = 'strict_hour_minute_second_fraction';
    const FORMAT_HOUR_MINUTE_SECOND_MILLIS = 'hour_minute_second_millis';
    const FORMAT_STRICT_HOUR_MINUTE_SECOND_MILLIS = 'strict_hour_minute_second_millis';
    const FORMAT_ORDINAL_DATE = 'ordinal_date';
    const FORMAT_STRICT_ORDINAL_DATE = 'strict_ordinal_date';
    const FORMAT_ORDINAL_DATE_TIME = 'ordinal_date_time';
    const FORMAT_STRICT_ORDINAL_DATE_TIME = 'strict_ordinal_date_time';
    const FORMAT_ORDINAL_DATE_TIME_NO_MILLIS = 'ordinal_date_time_no_millis';
    const FORMAT_STRICT_ORDINAL_DATE_TIME_NO_MILLIS = 'strict_ordinal_date_time_no_millis';
    const FORMAT_TIME = 'time';
    const FORMAT_STRICT_TIME = 'strict_time';
    const FORMAT_TIME_NO_MILLIS = 'time_no_millis';
    const FORMAT_STRICT_TIME_NO_MILLIS = 'strict_time_no_millis';
    const FORMAT_T_TIME = 't_time';
    const FORMAT_STRICT_T_TIME = 'strict_t_time';
    const FORMAT_T_TIME_NO_MILLIS = 't_time_no_millis';
    const FORMAT_STRICT_T_TIME_NO_MILLIS = 'strict_t_time_no_millis';
    const FORMAT_WEEK_DATE = 'week_date';
    const FORMAT_STRICT_WEEK_DATE = 'strict_week_date';
    const FORMAT_WEEK_DATE_TIME = 'week_date_time';
    const FORMAT_STRICT_WEEK_DATE_TIME = 'strict_week_date_time';
    const FORMAT_WEEK_DATE_TIME_NO_MILLIS = 'week_date_time_no_millis';
    const FORMAT_STRICT_WEEK_DATE_TIME_NO_MILLIS = 'strict_week_date_time_no_millis';
    const FORMAT_WEEKYEAR = 'weekyear';
    const FORMAT_STRICT_WEEKYEAR = 'strict_weekyear';
    const FORMAT_WEEKYEAR_WEEK = 'weekyear_week';
    const FORMAT_STRICT_WEEKYEAR_WEEK = 'strict_weekyear_week';
    const FORMAT_WEEKYEAR_WEEK_DAY = 'weekyear_week_day';
    const FORMAT_STRICT_WEEKYEAR_WEEK_DAY = 'strict_weekyear_week_day';
    const FORMAT_YEAR = 'year';
    const FORMAT_STRICT_YEAR = 'strict_year';
    const FORMAT_YEAR_MONTH = 'year_month';
    const FORMAT_STRICT_YEAR_MONTH = 'strict_year_month';
    const FORMAT_YEAR_MONTH_DAY = 'year_month_day';
    const FORMAT_STRICT_YEAR_MONTH_DAY = 'strict_year_month_day';

    protected $type;
    protected $format;
    protected $analyzer;
    protected $store;
    protected $index;
    protected $enabled;

    public function __construct ($type = null, $analyzer = null, $index = null, $store = null, $format = null, $enabled = null) {
        $this->setType($type);
        $this->setAnalyzer($analyzer);
        $this->setIndex($index);
        $this->setStore($store);
        $this->setFormat($format);
        $this->setEnabled($enabled);
    }

    public function getMappingArray () {
        $fields = [];

        if ($this->getType() !== null) {
            $fields['type'] = $this->getType();
        }

        if ($this->getFormat() !== null) {
            $fields['format'] = $this->getFormat();
        }

        if ($this->getAnalyzer() !== null) {
            $fields['analyzer'] = $this->getAnalyzer();
        }

        if ($this->getStore() !== null) {
            $fields['store'] = $this->getStore();
        }

        if ($this->getIndex() !== null) {
            $fields['index'] = $this->getIndex();
        }

        if ($this->getEnabled() !== null) {
            $fields['enabled'] = $this->getEnabled();
        }

        return $fields;
    }

    /**
     * @return string
     */
    public function getType () {
        return $this->type;
    }

    /**
     * @param string $type
     * @return Mapping
     */
    public function setType ($type) {
        $this->type = $type;
        return $this;
    }

    /**
     * @return string
     */
    public function getFormat () {
        return $this->format;
    }

    /**
     * @param string $format
     * @return Mapping
     */
    public function setFormat ($format) {
        $this->format = $format;
        return $this;
    }

    /**
     * @return string
     */
    public function getAnalyzer () {
        return $this->analyzer;
    }

    /**
     * @param string $analyzer
     * @return Mapping
     */
    public function setAnalyzer ($analyzer) {
        $this->analyzer = $analyzer;
        return $this;
    }

    /**
     * @return bool
     */
    public function getStore () {
        return $this->store;
    }

    /**
     * @param bool $store
     * @return Mapping
     */
    public function setStore ($store) {
        $this->store = $store;
        return $this;
    }

    /**
     * @return bool
     */
    public function getIndex () {
        return $this->index;
    }

    /**
     * @param bool $index
     * @return Mapping
     */
    public function setIndex ($index) {
        $this->index = $index;
        return $this;
    }

    /**
     * @return bool
     */
    public function getEnabled () {
        return $this->enabled;
    }

    /**
     * @param bool $enabled
     * @return Mapping
     */
    public function setEnabled ($enabled) {
        $this->enabled = $enabled;
        return $this;
    }
}