<?php
namespace Updashd\SearchIndexer\Document;

use Updashd\SearchIndexer\Mapping;

abstract class AbstractDocument {
    protected $name;
    protected $mapping = [];

    /**
     * Get the list of changes that have been made that need to be indexed.
     * This should return an array of associative arrays with key => value mappings.
     * All rows must have:
     * - date_time field of type \DateTime for tracking the query.
     * - account_id field of type int for permissions
     * - account_name for admin info
     * If default mapping is used, every row should have an id, name, auto_complete, type, and url.
     * Example:
     * [
     *     [
     *         'id' => 1,
     *         'name' => 'Test',
     *         'account_id' => 1,
     *         'account_name' => 'Custom Account',
     *         'date_time' => new \DateTime(...)
     *     ],
     *     ...
     * ]
     * @return array
     */
    abstract public function getUpserts ();

    /**
     * Get the list of deletions that need to be made.
     * This should return an array of associative arrays with key => value mappings.
     * All rows must have:
     * - id
     * Example:
     * [
     *     [
     *         'id' => 1,
     *     ],
     *     ...
     * ]
     * @return array
     */
    abstract public function getDeletes ();

    /**
     * AbstractEntity constructor.
     * @param string $name
     * @param Mapping[] $mapping Associative array of name to Field
     * @param bool $includeDefault whether to include the default fields in the mapping. It will be merged by key.
     */
    public function __construct ($name, $mapping = [], $includeDefault = true) {
        $this->setName($name);

        if ($includeDefault) {
            $defaultMapping = [
                'account_id' => (new Mapping(Mapping::TYPE_NUMERIC_INTEGER)),
                'account_name' => (new Mapping(Mapping::TYPE_STRING_TEXT)),
                'account_slug' => (new Mapping(Mapping::TYPE_STRING_KEYWORD)),
                'date_time' => (new Mapping(Mapping::TYPE_DATE))
                    ->setIndex(false)
                    ->setFormat(Mapping::FORMAT_STRICT_DATE_TIME_NO_MILLIS),
                'id' => new Mapping(Mapping::TYPE_NUMERIC_INTEGER),
                'name' => new Mapping(Mapping::TYPE_STRING_KEYWORD),
                'name_english' => new Mapping(Mapping::TYPE_STRING_TEXT, Mapping::ANALYZER_LANGUAGE_ENGLISH),
                'type' => new Mapping(Mapping::TYPE_STRING_KEYWORD),
                'url' => (new Mapping(Mapping::TYPE_OBJECT))
                    ->setEnabled(false),
            ];

            $mapping = $mapping + $defaultMapping;
        }

        $this->setMapping($mapping);
    }

    public function getMappingArray () {
        $result = [];

        /**
         * @var string $field
         * @var Mapping $mapping
         */
        foreach ($this->getMapping() as $field => $mapping) {
            $result[$field] = $mapping->getMappingArray();
        }

        return $result;
    }

    /**
     * @return string
     */
    public function getName () {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName ($name) {
        $this->name = $name;
    }

    /**
     * @return Mapping[]
     */
    protected function getMapping () {
        return $this->mapping;
    }

    /**
     * @param Mapping[] $mapping
     */
    protected function setMapping ($mapping) {
        $this->mapping = $mapping;
    }
}