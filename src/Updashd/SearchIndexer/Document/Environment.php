<?php
namespace Updashd\SearchIndexer\Document;

use Doctrine\ORM\EntityManager;
use Updashd\DbEvent\PopulateStatement;
use Updashd\DbEvent\PopulateStatements;
use Updashd\DbEvent\Processor;
use Updashd\DbEvent\Subscriptions;
use Updashd\SearchIndexer\Mapping;

class Environment extends AbstractDoctrineDocument {
    const NAME = 'Environment';
    const CLIENT_ID = 'IDX_ENV';

    public function __construct (EntityManager $entityManager) {
        parent::__construct($entityManager, 'environment', [
            // Environment Properties
            'is_enabled' => (new Mapping(Mapping::TYPE_BOOLEAN)),
            'bootstrap_color' => (new Mapping(Mapping::TYPE_STRING_KEYWORD)),
            'layout_color' => (new Mapping(Mapping::TYPE_STRING_KEYWORD)),
            'sort_order' => (new Mapping(Mapping::TYPE_NUMERIC_INTEGER)),
        ]);
    }

    public function getUpdateProcessor () {
        $subscriptions = new Subscriptions();
        $subscriptions->addSubscriptionAuto('account', ['U']);
        $subscriptions->addSubscriptionAuto('environment', ['I', 'U']);

        $updatePopulateStatements = new PopulateStatements();
        $updatePopulateStatements
            ->addStatement(new PopulateStatement(
                'e.environment_id',
                'account',
                null,
                [
                    'INNER JOIN environment e ON a.new_id = e.account_id'
                ],
                ['U']
            ))
            ->addStatement(new PopulateStatement(
                'a.new_id',
                'environment',
                null,
                [],
                ['I', 'U']
            ));


        $updateProcessor = new Processor($this->getPdo(), self::CLIENT_ID, 'tmp_idx_env_u', $subscriptions);
        $updateProcessor->setPopulateStatements($updatePopulateStatements);

        return $updateProcessor;
    }

    /**
     * Get the list of changes that have been made that need to be indexed.
     * This should return an array of associative arrays with key => value mappings.
     * All rows must have:
     * - date_time field of type \DateTime for tracking the query.
     * - account_id field of type int for permissions
     * - account_name for admin info
     * If default mapping is used, every row should have an id, name, auto_complete, type, and url.
     * Example:
     * [
     *     [
     *         'id' => 1,
     *         'name' => 'Test',
     *         'account_id' => 1,
     *         'account_name' => 'Custom Account',
     *         'date_time' => new \DateTime(...)
     *     ],
     *     ...
     * ]
     * @return array
     */
    public function getUpserts () {
        $updateProcessor = $this->getUpdateProcessor();

        $updateProcessor->populate();

        $result = $updateProcessor->retrieve(
            [
                'acc.account_id',
                'acc.slug AS account_slug',
                'acc.name AS account_name',
                'e.updated_date AS e_updated_date',
                'e.environment_id AS id',
                'e.environment_name AS name',
                'e.sort_order',
            ],
            [
                'INNER JOIN environment e ON r.root_id = e.environment_id',
                'INNER JOIN account acc ON e.account_id = acc.account_id',
            ],
            'environment_id',
            $this->getUpdateAll() ? 'environment' : null
        );

        $updateProcessor->finalize();

        // Process the results
        foreach ($result as $key => $row) {
            //////////////////
            // Standard Fields
            //////////////////

            $name = $row['name'];

            $result[$key]['date_time'] = $this->formatDate($row['e_updated_date']);
            $result[$key]['name_english'] = $name;
            $result[$key]['auto_complete'] = $name;
            $result[$key]['type'] = self::NAME;
            $result[$key]['url'] = [
                'route' => 'account',
                'controller' => 'environment',
                'action' => 'edit',
                'route_params' => ['account' => $row['account_slug']],
                'get_params' => ['eid' => $row['id']]
            ];

            ////////////////
            // Custom Fields
            ////////////////

            $this->filterFields($result[$key]);
        }

        return $result;
    }

    public function getDeletes () {
        return $this->getDeletesAuto('environment', self::CLIENT_ID, 'tmp_idx_env_d');
    }
}