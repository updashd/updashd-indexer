<?php
namespace Updashd\SearchIndexer\Document;

use Doctrine\ORM\EntityManager;
use Updashd\DbEvent\PopulateStatement;
use Updashd\DbEvent\PopulateStatements;
use Updashd\DbEvent\Processor;
use Updashd\DbEvent\Subscriptions;
use Updashd\SearchIndexer\Mapping;

class AccountNotifier extends AbstractDoctrineDocument {
    const NAME = 'Notifier';
    const CLIENT_ID = 'IDX_ACC';

    /**
     * @param EntityManager $entityManager
     */
    public function __construct (EntityManager $entityManager) {
        parent::__construct($entityManager, 'account_notifier', [
            // Account Notifier properties
            'is_enabled' => (new Mapping(Mapping::TYPE_BOOLEAN)),
            'is_default' => (new Mapping(Mapping::TYPE_BOOLEAN)),
            'sort_order' => (new Mapping(Mapping::TYPE_NUMERIC_INTEGER)),
            'frequency' => (new Mapping(Mapping::TYPE_NUMERIC_LONG)),
            'config' => (new Mapping(Mapping::TYPE_OBJECT)),

            // Referenced Properties
            'notifier_id' => (new Mapping(Mapping::TYPE_NUMERIC_LONG)),
            'notifier_name' => (new Mapping(Mapping::TYPE_STRING_KEYWORD)),
            'notifier_name_english' => (new Mapping(Mapping::TYPE_STRING_TEXT, Mapping::ANALYZER_LANGUAGE_ENGLISH)),
            'notifier_module_name' => (new Mapping(Mapping::TYPE_STRING_KEYWORD)),
            'notifier_sort_order' => (new Mapping(Mapping::TYPE_NUMERIC_INTEGER)),
        ]);
    }

    /**
     * @return Processor
     */
    public function getUpdateProcessor () {
        $subscriptions = new Subscriptions();
        $subscriptions->addSubscriptionAuto('account', ['U']);
        $subscriptions->addSubscriptionAuto('notifier', ['U']);
        $subscriptions->addSubscriptionAuto('account_notifier', ['I', 'U']);

        $updatePopulateStatements = new PopulateStatements();
        $updatePopulateStatements
            ->addStatement(new PopulateStatement(
                'an.account_notifier_id',
                'account',
                null,
                [
                    'INNER JOIN account_notifier an ON a.new_id = an.account_id'
                ],
                ['U']
            ))
            ->addStatement(new PopulateStatement(
                'an.account_notifier_id',
                'notifier',
                null,
                [
                    'INNER JOIN account_notifier an ON a.new_id = an.notifier_id'
                ],
                ['U']
            ))
            ->addStatement(new PopulateStatement(
                'a.new_id',
                'account_notifier',
                null,
                [],
                ['I', 'U']
            ));

        $updateProcessor = new Processor($this->getPdo(), self::CLIENT_ID, 'tmp_idx_acc_u', $subscriptions);
        $updateProcessor->setPopulateStatements($updatePopulateStatements);

        return $updateProcessor;
    }

    /**
     * @return array
     */
    public function getUpserts () {
        $updateProcessor = $this->getUpdateProcessor();

        $updateProcessor->populate();

        $result = $updateProcessor->retrieve(
            [
                'acc.account_id',
                'acc.slug AS account_slug',
                'acc.name AS account_name',
                'an.updated_date AS an_updated_date',
                'an.account_notifier_id AS id',
                'an.account_notifier_name AS name',
                'an.config',
                'an.sort_order',
                'nf.notifier_id',
                'nf.notifier_name',
                'nf.module_name AS notifier_module_name',
                'nf.sort_order AS notifier_sort_order'
            ],
            [
                'INNER JOIN account_notifier an ON r.root_id = an.account_notifier_id',
                'INNER JOIN account acc ON an.account_id = acc.account_id',
                'INNER JOIN notifier nf ON an.notifier_id = nf.notifier_id'
            ],
            'account_notifier_id',
            $this->getUpdateAll() ? 'account_notifier' : null
        );

        $updateProcessor->finalize();

        // Process the results
        foreach ($result as $key => $row) {
            //////////////////
            // Standard Fields
            //////////////////

            $name = $row['name'];

            $result[$key]['date_time'] = $this->formatDate($row['an_updated_date']);
            $result[$key]['name_english'] = $name;
            $result[$key]['auto_complete'] = $name;
            $result[$key]['type'] = self::NAME;
            $result[$key]['url'] = [
                'route' => 'account',
                'controller' => 'notifier',
                'action' => 'edit',
                'route_params' => ['account' => $row['account_slug']],
                'get_params' => ['nid' => $row['id']]
            ];

            ////////////////
            // Custom Fields
            ////////////////

            $result[$key]['notifier_name_english'] = $row['notifier_name'];

            $result[$key]['config'] = json_decode($row['config'], true);

            $this->filterFields($result[$key]);
        }

        return $result;
    }

    public function getDeletes () {
        return $this->getDeletesAuto('account_notifier', self::CLIENT_ID, 'tmp_idx_ano_d');
    }
}