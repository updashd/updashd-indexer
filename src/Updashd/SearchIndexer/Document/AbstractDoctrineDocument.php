<?php
namespace Updashd\SearchIndexer\Document;

use Doctrine\ORM\EntityManager;
use Updashd\DbEvent\PopulateStatement;
use Updashd\DbEvent\PopulateStatements;
use Updashd\DbEvent\Processor;
use Updashd\DbEvent\Subscriptions;

abstract class AbstractDoctrineDocument extends AbstractDocument {
    const ES_DATE_TIME_FORMAT = 'Y-m-d\TH:i:s\Z';

    protected $entityManager;
    protected $updateAll = false;

    /**
     * AbstractDoctrineEntity constructor.
     * @param EntityManager $entityManager
     * @param string $name
     * @param array $mapping
     * @param bool $includeDefault
     * @param bool $updateAll
     */
    public function __construct ($entityManager, $name, array $mapping = [], $includeDefault = true, $updateAll = false) {
        $this->setEntityManager($entityManager);
        $this->setUpdateAll($updateAll);

        parent::__construct($name, $mapping, $includeDefault);
    }

    protected function formatDate ($date) {
        $date = new \DateTime($date, new \DateTimeZone('UTC'));
        return $date->format(self::ES_DATE_TIME_FORMAT);
    }

    protected function filterFields (&$row) {
        $mappingFields = $this->getMapping();
        $mappingFields = array_keys($mappingFields);
        foreach ($row as $field => $value) {
            // If the field is not found in the mapped fields
            if (! in_array($field, $mappingFields) || empty($value)) {
                // Remove it
                unset($row[$field]);
            }
        }
    }

    protected function getPdo () {
        $em = $this->getEntityManager();
        return $em->getConnection();
    }

    protected function generateDeleteProcessor ($tableName, $clientId, $tmpTableName = null) {
        $subscriptions = new Subscriptions();
        $subscriptions->addSubscriptionAuto($tableName, ['D']);

        $deletePopulateStatements = new PopulateStatements();
        $deletePopulateStatements
            ->addStatement(new PopulateStatement(
                'a.old_id',
                $tableName,
                null,
                [],
                ['D']
            ));

        if (! $tmpTableName) {
            $tmpTableName = 'tmp_idx_' . $tableName . '_d';
        }

        $deleteProcessor = new Processor($this->getPdo(), $clientId, $tmpTableName, $subscriptions);
        $deleteProcessor->setPopulateStatements($deletePopulateStatements);

        return $deleteProcessor;
    }

    protected function getDeletesAuto ($tableName, $clientId, $tmpTableName = null) {
        $deleteProc = $this->generateDeleteProcessor($tableName, $clientId, $tmpTableName);

        $deleteProc->populate();

        $result = $deleteProc->retrieve(
            [],
            'id'
        );

        $deleteProc->finalize();

        return $result;
    }

    /**
     * @return EntityManager
     */
    public function getEntityManager () {
        return $this->entityManager;
    }

    /**
     * @param EntityManager $entityManager
     */
    public function setEntityManager ($entityManager) {
        $this->entityManager = $entityManager;
    }

    /**
     * @return bool
     */
    public function getUpdateAll () {
        return $this->updateAll;
    }

    /**
     * @param bool $updateAll
     */
    public function setUpdateAll ($updateAll = true) {
        $this->updateAll = $updateAll;
    }
}