<?php
namespace Updashd\SearchIndexer\Document;

use Doctrine\ORM\EntityManager;
use Updashd\DbEvent\PopulateStatement;
use Updashd\DbEvent\PopulateStatements;
use Updashd\DbEvent\Processor;
use Updashd\DbEvent\Subscriptions;
use Updashd\SearchIndexer\Mapping;

class Node extends AbstractDoctrineDocument {
    const NAME = 'Node';
    const CLIENT_ID = 'IDX_NDE';

    public function __construct (EntityManager $entityManager) {
        parent::__construct($entityManager, 'node', [
            // Node Properties
            'is_enabled' => (new Mapping(Mapping::TYPE_BOOLEAN)),
            'hostname' => (new Mapping(Mapping::TYPE_STRING_KEYWORD)),
            'ip' => (new Mapping(Mapping::TYPE_IP)),
            'sort_order' => (new Mapping(Mapping::TYPE_NUMERIC_INTEGER)),

            // Referenced Properties
            'environment_id' => (new Mapping(Mapping::TYPE_NUMERIC_LONG)),
            'environment_name' => (new Mapping(Mapping::TYPE_STRING_KEYWORD)),
            'environment_name_english' => (new Mapping(Mapping::TYPE_STRING_TEXT, Mapping::ANALYZER_LANGUAGE_ENGLISH)),
            'environment_sort_order' => (new Mapping(Mapping::TYPE_NUMERIC_INTEGER)),
        ]);
    }

    public function getUpdateProcessor () {
        $subscriptions = new Subscriptions();
        $subscriptions->addSubscriptionAuto('account', ['U']);
        $subscriptions->addSubscriptionAuto('node', ['I', 'U']);

        $updatePopulateStatements = new PopulateStatements();
        $updatePopulateStatements
            ->addStatement(new PopulateStatement(
                'n.node_id',
                'account',
                null,
                [
                    'INNER JOIN node n ON a.new_id = n.account_id'
                ],
                ['U']
            ))
            ->addStatement(new PopulateStatement(
                'n.node_id',
                'environment',
                null,
                [
                    'INNER JOIN node n ON a.new_id = n.environment_id'
                ],
                ['U']
            ))
            ->addStatement(new PopulateStatement(
                'a.new_id',
                'node',
                null,
                [],
                ['I', 'U']
            ));


        $updateProcessor = new Processor($this->getPdo(), self::CLIENT_ID, 'tmp_idx_nde_u', $subscriptions);
        $updateProcessor->setPopulateStatements($updatePopulateStatements);

        return $updateProcessor;
    }

    /**
     * Get the list of changes that have been made that need to be indexed.
     * This should return an array of associative arrays with key => value mappings.
     * All rows must have:
     * - date_time field of type \DateTime for tracking the query.
     * - account_id field of type int for permissions
     * - account_name for admin info
     * If default mapping is used, every row should have an id, name, auto_complete, type, and url.
     * Example:
     * [
     *     [
     *         'id' => 1,
     *         'name' => 'Test',
     *         'account_id' => 1,
     *         'account_name' => 'Custom Account',
     *         'date_time' => new \DateTime(...)
     *     ],
     *     ...
     * ]
     * @return array
     */
    public function getUpserts () {
        $updateProcessor = $this->getUpdateProcessor();

        $updateProcessor->populate();

        $result = $updateProcessor->retrieve(
            [
                'acc.account_id',
                'acc.slug AS account_slug',
                'acc.name AS account_name',
                'n.updated_date AS n_updated_date',
                'n.node_id AS id',
                'CONCAT(n.node_name, " (", e.environment_name, ")") AS name',
                'n.hostname',
                'n.ip',
                'n.sort_order',
                'e.environment_id AS environment_id',
                'e.environment_name AS environment_name',
                'e.sort_order AS environment_sort_order',
            ],
            [
                'INNER JOIN node n ON r.root_id = n.node_id',
                'INNER JOIN environment e ON n.environment_id = e.environment_id',
                'INNER JOIN account acc ON n.account_id = acc.account_id',
            ],
            'node_id',
            $this->getUpdateAll() ? 'node' : null
        );

        $updateProcessor->finalize();

        // Process the results
        foreach ($result as $key => $row) {
            //////////////////
            // Standard Fields
            //////////////////

            $name = $row['name'];

            $result[$key]['date_time'] = $this->formatDate($row['n_updated_date']);
            $result[$key]['name_english'] = $name;
            $result[$key]['auto_complete'] = $name;
            $result[$key]['type'] = self::NAME;
            $result[$key]['url'] = [
                'route' => 'account',
                'controller' => 'node',
                'action' => 'edit',
                'route_params' => ['account' => $row['account_slug']],
                'get_params' => ['nid' => $row['id']]
            ];

            ////////////////
            // Custom Fields
            ////////////////
            $result[$key]['environment_name_english'] = $row['environment_name'];

            $this->filterFields($result[$key]);
        }

        return $result;
    }

    public function getDeletes () {
        return $this->getDeletesAuto('node', self::CLIENT_ID, 'tmp_idx_nde_d');
    }
}