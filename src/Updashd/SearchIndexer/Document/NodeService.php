<?php
namespace Updashd\SearchIndexer\Document;

use Doctrine\ORM\EntityManager;
use Updashd\DbEvent\PopulateStatement;
use Updashd\DbEvent\PopulateStatements;
use Updashd\DbEvent\Processor;
use Updashd\DbEvent\Subscriptions;
use Updashd\SearchIndexer\Mapping;

class NodeService extends AbstractDoctrineDocument {
    const NAME = 'Node Service';
    const CLIENT_ID = 'IDX_NS';

    public function __construct (EntityManager $entityManager) {
        parent::__construct($entityManager, 'node_service', [
            // Node Properties
            'is_enabled' => (new Mapping(Mapping::TYPE_BOOLEAN)),
            'sort_order' => (new Mapping(Mapping::TYPE_NUMERIC_INTEGER)),
            'color' => (new Mapping(Mapping::TYPE_STRING_KEYWORD)),

            // Referenced Properties
            'node_id' => (new Mapping(Mapping::TYPE_NUMERIC_LONG)),
            'node_name' => (new Mapping(Mapping::TYPE_STRING_KEYWORD)),
            'node_name_english' => (new Mapping(Mapping::TYPE_STRING_TEXT, Mapping::ANALYZER_LANGUAGE_ENGLISH)),
            'node_sort_order' => (new Mapping(Mapping::TYPE_NUMERIC_INTEGER)),
            'node_hostname' => (new Mapping(Mapping::TYPE_STRING_KEYWORD)),
            'node_ip' => (new Mapping(Mapping::TYPE_IP)),

            'service_id' => (new Mapping(Mapping::TYPE_NUMERIC_LONG)),
            'service_name' => (new Mapping(Mapping::TYPE_STRING_KEYWORD)),
            'service_name_english' => (new Mapping(Mapping::TYPE_STRING_TEXT, Mapping::ANALYZER_LANGUAGE_ENGLISH)),
            'service_sort_order' => (new Mapping(Mapping::TYPE_NUMERIC_INTEGER)),

            'environment_id' => (new Mapping(Mapping::TYPE_NUMERIC_LONG)),
            'environment_name' => (new Mapping(Mapping::TYPE_STRING_KEYWORD)),
            'environment_name_english' => (new Mapping(Mapping::TYPE_STRING_TEXT, Mapping::ANALYZER_LANGUAGE_ENGLISH)),
            'environment_sort_order' => (new Mapping(Mapping::TYPE_NUMERIC_INTEGER)),
        ]);
    }

    public function getUpdateProcessor () {
        $subscriptions = new Subscriptions();
        $subscriptions->addSubscriptionAuto('account', ['U']);
        $subscriptions->addSubscriptionAuto('node_service', ['I', 'U']);
        $subscriptions->addSubscriptionAuto('service', ['U']);
        $subscriptions->addSubscriptionAuto('node', ['U']);
        $subscriptions->addSubscriptionAuto('environment', ['U']);

        $updatePopulateStatements = new PopulateStatements();
        $updatePopulateStatements
            ->addStatement(new PopulateStatement(
                'ns.node_service_id',
                'account',
                null,
                [
                    'INNER JOIN node n ON a.new_id = n.account_id',
                    'INNER JOIN node_service ns ON n.node_id = ns.node_id',
                ],
                ['U']
            ))
            ->addStatement(new PopulateStatement(
                'ns.node_service_id',
                'environment',
                null,
                [
                    'INNER JOIN node n ON a.new_id = n.environment_id',
                    'INNER JOIN node_service ns ON n.node_id = ns.node_id',
                ],
                ['U']
            ))
            ->addStatement(new PopulateStatement(
                'ns.node_service_id',
                'service',
                null,
                [
                    'INNER JOIN node_service ns ON a.new_id = ns.service_id',
                ],
                ['U']
            ))
            ->addStatement(new PopulateStatement(
                'a.new_id',
                'node_service',
                null,
                [],
                ['I', 'U']
            ));


        $updateProcessor = new Processor($this->getPdo(), self::CLIENT_ID, 'tmp_idx_ns_u', $subscriptions);
        $updateProcessor->setPopulateStatements($updatePopulateStatements);

        return $updateProcessor;
    }

    /**
     * Get the list of changes that have been made that need to be indexed.
     * This should return an array of associative arrays with key => value mappings.
     * All rows must have:
     * - date_time field of type \DateTime for tracking the query.
     * - account_id field of type int for permissions
     * - account_name for admin info
     * If default mapping is used, every row should have an id, name, auto_complete, type, and url.
     * Example:
     * [
     *     [
     *         'id' => 1,
     *         'name' => 'Test',
     *         'account_id' => 1,
     *         'account_name' => 'Custom Account',
     *         'date_time' => new \DateTime(...)
     *     ],
     *     ...
     * ]
     * @return array
     */
    public function getUpserts () {
        $updateProcessor = $this->getUpdateProcessor();

        $updateProcessor->populate();

        $result = $updateProcessor->retrieve(
            [
                'acc.account_id',
                'acc.slug AS account_slug',
                'acc.name AS account_name',

                'ns.updated_date AS ns_updated_date',
                'ns.node_service_id AS id',
                'ns.node_service_name AS name',
                'CONCAT(ns.node_service_name, "—", n.node_name, " (", e.environment_name, ")") AS name',
                'ns.sort_order',

                'n.node_id',
                'n.node_name',
                'n.sort_order AS node_sort_order',
                'n.hostname AS node_hostname',
                'n.ip AS node_ip',

                's.service_id',
                's.service_name',
                's.sort_order AS service_sort_order',

                'e.environment_id',
                'e.environment_name',
                'e.sort_order AS environment_sort_order',
            ],
            [
                'INNER JOIN node_service ns ON r.root_id = ns.node_service_id',
                'INNER JOIN node n ON ns.node_id = n.node_id',
                'INNER JOIN service s ON ns.service_id = s.service_id',
                'INNER JOIN environment e ON n.environment_id = e.environment_id',
                'INNER JOIN account acc ON n.account_id = acc.account_id',
            ],
            'node_service_id',
            $this->getUpdateAll() ? 'node_service' : null
        );

        $updateProcessor->finalize();

        // Process the results
        foreach ($result as $key => $row) {
            //////////////////
            // Standard Fields
            //////////////////

            $name = $row['name'];

            $result[$key]['date_time'] = $this->formatDate($row['ns_updated_date']);
            $result[$key]['name_english'] = $name;
            $result[$key]['auto_complete'] = $name;
            $result[$key]['type'] = self::NAME;
            $result[$key]['url'] = [
                'route' => 'account',
                'controller' => 'node',
                'action' => 'service_basic',
                'route_params' => ['account' => $row['account_slug']],
                'get_params' => ['nsid' => $row['id']]
            ];

            ////////////////
            // Custom Fields
            ////////////////
            $result[$key]['node_name_english'] = $row['node_name'];
            $result[$key]['environment_name_english'] = $row['environment_name'];
            $result[$key]['service_name_english'] = $row['service_name'];

            $this->filterFields($result[$key]);
        }

        return $result;
    }

    public function getDeletes () {
        return $this->getDeletesAuto('node', self::CLIENT_ID, 'tmp_idx_nde_d');
    }
}