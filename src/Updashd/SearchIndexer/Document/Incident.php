<?php
namespace Updashd\SearchIndexer\Document;

use Doctrine\ORM\EntityManager;
use Updashd\DbEvent\PopulateStatement;
use Updashd\DbEvent\PopulateStatements;
use Updashd\DbEvent\Processor;
use Updashd\DbEvent\Subscriptions;
use Updashd\SearchIndexer\Mapping;

class Incident extends AbstractDoctrineDocument {
    const NAME = 'Incident';
    const CLIENT_ID = 'IDX_INC';

    public function __construct (EntityManager $entityManager) {
        parent::__construct($entityManager, 'incident', [
            // Incident Properties
            'date_first_seen' => (new Mapping(Mapping::TYPE_DATE)),
            'date_last_seen' => (new Mapping(Mapping::TYPE_DATE)),
            'severity' => (new Mapping(Mapping::TYPE_STRING_KEYWORD)),
            'message_code' => (new Mapping(Mapping::TYPE_NUMERIC_LONG)),
            'is_read' => (new Mapping(Mapping::TYPE_BOOLEAN)),
            'is_resolved' => (new Mapping(Mapping::TYPE_BOOLEAN)),

            // Node Service Properties
            'node_service_id' => (new Mapping(Mapping::TYPE_NUMERIC_LONG)),
            'node_service_name' => (new Mapping(Mapping::TYPE_STRING_KEYWORD)),
            'node_service_name_english' => (new Mapping(Mapping::TYPE_STRING_TEXT, Mapping::ANALYZER_LANGUAGE_ENGLISH)),

            // Node Properties
            'node_id' => (new Mapping(Mapping::TYPE_NUMERIC_LONG)),
            'node_name' => (new Mapping(Mapping::TYPE_STRING_KEYWORD)),
            'node_name_english' => (new Mapping(Mapping::TYPE_STRING_TEXT, Mapping::ANALYZER_LANGUAGE_ENGLISH)),

            // Service Properties
            'service_id' => (new Mapping(Mapping::TYPE_NUMERIC_LONG)),
            'service_name' => (new Mapping(Mapping::TYPE_STRING_KEYWORD)),
            'service_name_english' => (new Mapping(Mapping::TYPE_STRING_TEXT, Mapping::ANALYZER_LANGUAGE_ENGLISH)),

            // Environment Properties
            'environment_id' => (new Mapping(Mapping::TYPE_NUMERIC_LONG)),
            'environment_name' => (new Mapping(Mapping::TYPE_STRING_KEYWORD)),
            'environment_name_english' => (new Mapping(Mapping::TYPE_STRING_TEXT, Mapping::ANALYZER_LANGUAGE_ENGLISH)),
        ]);
    }

    public function getUpdateProcessor () {
        $subscriptions = new Subscriptions();
        $subscriptions->addSubscriptionAuto('account', ['U']);
        $subscriptions->addSubscriptionAuto('node', ['U']);
        $subscriptions->addSubscriptionAuto('service', ['U']);
        $subscriptions->addSubscriptionAuto('node_service', ['U']);
        $subscriptions->addSubscriptionAuto('environment', ['U']);
        $subscriptions->addSubscriptionAuto('incident', ['I', 'U']);

        $updatePopulateStatements = new PopulateStatements();
        $updatePopulateStatements
            ->addStatement(new PopulateStatement(
                'i.incident_id',
                'account',
                null,
                [
                    'INNER JOIN node n ON a.new_id = n.account_id',
                    'INNER JOIN node_service ns ON n.node_id = ns.node_id',
                    'INNER JOIN incident i ON ns.node_service_id = i.node_service_id'
                ],
                ['U']
            ))
            ->addStatement(new PopulateStatement(
                'i.incident_id',
                'node',
                null,
                [
                    'INNER JOIN node_service ns ON a.new_id = ns.node_id',
                    'INNER JOIN incident i ON ns.node_service_id = i.node_service_id'
                ],
                ['U']
            ))
            ->addStatement(new PopulateStatement(
                'i.incident_id',
                'service',
                null,
                [
                    'INNER JOIN node_service ns ON a.new_id = ns.service_id',
                    'INNER JOIN incident i ON ns.node_service_id = i.node_service_id'
                ],
                ['U']
            ))
            ->addStatement(new PopulateStatement(
                'i.incident_id',
                'environment',
                null,
                [
                    'INNER JOIN node n ON a.new_id = n.environment_id',
                    'INNER JOIN node_service ns ON n.node_id = ns.node_id',
                    'INNER JOIN incident i ON ns.node_service_id = i.node_service_id'
                ],
                ['U']
            ))
            ->addStatement(new PopulateStatement(
                'i.incident_id',
                'node_service',
                null,
                [
                    'INNER JOIN incident i ON a.new_id = i.node_service_id'
                ],
                ['U']
            ))
            ->addStatement(new PopulateStatement(
                'a.new_id',
                'incident',
                null,
                [],
                ['I', 'U']
            ));


        $updateProcessor = new Processor($this->getPdo(), self::CLIENT_ID, 'tmp_idx_inc_u', $subscriptions);
        $updateProcessor->setPopulateStatements($updatePopulateStatements);

        return $updateProcessor;
    }

    /**
     * Get the list of changes that have been made that need to be indexed.
     * This should return an array of associative arrays with key => value mappings.
     * All rows must have:
     * - date_time field of type \DateTime for tracking the query.
     * - account_id field of type int for permissions
     * - account_name for admin info
     * If default mapping is used, every row should have an id, name, auto_complete, type, and url.
     * Example:
     * [
     *     [
     *         'id' => 1,
     *         'name' => 'Test',
     *         'account_id' => 1,
     *         'account_name' => 'Custom Account',
     *         'date_time' => new \DateTime(...)
     *     ],
     *     ...
     * ]
     * @return array
     */
    public function getUpserts () {
        $updateProcessor = $this->getUpdateProcessor();

        $updateProcessor->populate();

        $result = $updateProcessor->retrieve(
            [
                'acc.account_id',
                'acc.slug AS account_slug',
                'acc.name AS account_name',
                'i.updated_date AS i_updated_date',
                'i.incident_id AS id',
                'i.message AS name',
                'i.date_first_seen',
                'i.date_last_seen',
                'i.message_code',
                'i.is_read',
                'i.is_resolved',
                'n.node_id',
                'n.node_name',
                's.service_id',
                's.service_name',
                'ns.node_service_id',
                'ns.node_service_name',
                'e.environment_id',
                'e.environment_name',
            ],
            [
                'INNER JOIN incident i ON r.root_id = i.incident_id',
                'INNER JOIN node_service ns ON i.node_service_id = ns.node_service_id',
                'INNER JOIN node n ON ns.node_id = n.node_id',
                'INNER JOIN environment e ON n.environment_id = e.environment_id',
                'INNER JOIN service s ON ns.service_id = s.service_id',
                'INNER JOIN account acc ON e.account_id = acc.account_id',
            ],
            'incident_id',
            $this->getUpdateAll() ? 'incident' : null
        );

        $updateProcessor->finalize();

        // Process the results
        foreach ($result as $key => $row) {
            //////////////////
            // Standard Fields
            //////////////////

            $name = $row['name'];

            $result[$key]['date_time'] = $this->formatDate($row['i_updated_date']);
            $result[$key]['name_english'] = $name;
            $result[$key]['auto_complete'] = $name;
            $result[$key]['type'] = self::NAME;
            $result[$key]['url'] = [
                'route' => 'account',
                'controller' => 'incident',
                'action' => 'detail',
                'route_params' => ['account' => $row['account_slug']],
                'get_params' => ['iid' => $row['id']]
            ];

            ////////////////
            // Custom Fields
            ////////////////

            $result[$key]['date_first_seen'] = $this->formatDate($result[$key]['date_first_seen']);
            $result[$key]['date_last_seen'] = $this->formatDate($result[$key]['date_last_seen']);
            $result[$key]['node_service_name_english'] = $result[$key]['node_service_name'];
            $result[$key]['node_service_name_english'] = $result[$key]['node_service_name'];
            $result[$key]['node_name_english'] = $result[$key]['node_name'];
            $result[$key]['service_name_english'] = $result[$key]['service_name'];
            $result[$key]['environment_name_english'] = $result[$key]['environment_name'];

            $this->filterFields($result[$key]);
        }

        return $result;
    }

    public function getDeletes () {
        return $this->getDeletesAuto('incident', self::CLIENT_ID, 'tmp_idx_inc_d');
    }
}