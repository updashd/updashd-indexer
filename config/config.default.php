<?php
return [
    'frequency' => 10,
    'elasticsearch' => [
        'Hosts' => [
            getenv('ELASTICSEARCH_HOST') ?: 'elasticsearch'
        ]
    ],
    'doctrine' => [
        'options' => [
            'dev_mode' => getenv('UPDASHD_DEBUG') ?: false,
            'simple_annotation' => false,
            'entity_paths' => [
                __DIR__ . '/../vendor/updashd/updashd-model/src'
            ],
            'proxy_dir' => __DIR__ . '/../data/doctrine_proxy'
        ],
        'connection' => [
            'driver' => 'pdo_mysql',
            'host' => getenv('MYSQL_HOST') ?: 'localhost',
            'dbname' => getenv('MYSQL_DB') ?: 'updashd',
            'user' => getenv('MYSQL_USER') ?: 'updashd',
            'port' => getenv('MYSQL_PORT') ?: 3306,
            'password' => getenv('MYSQL_PASS') ?: null,
        ]
    ]
];